from argparse import ArgumentParser
import cv2
from vke.face import imgGetFaceKeypoints, imgGetFaceBbox
from vke.body import imgGetBodyKeypoints, imageDisplayBodyParts, imgGetBodyBbox
from nwutils.bbox import bbox_image_plot
from nwutils.kps import frame_keypointer

def get_args():
	parser = ArgumentParser()
	parser.add_argument("--body", type=int, default=1)
	parser.add_argument("--face", type=int, default=1)
	args = parser.parse_args()
	args.body = bool(args.body)
	args.face = bool(args.face)
	assert args.body + args.face > 0
	return args

def main():
	args = get_args()
	cap = cv2.VideoCapture(0)
	_, frame = cap.read()
	frame_height, frame_width = frame.shape[:2]

	while True:
		# Read frame
		_, readFrame = cap.read()
		readFrame = readFrame[:, 80:-80]
		readFrame = cv2.flip(readFrame, 1)
		image = readFrame[..., ::-1]
		valid_face = True
		valid_body = True

		# If no person is detected via YOLO, skip the frame.

		if args.body:
			imgBodyBbox = imgGetBodyBbox(image, "yolov5s")
			valid_body = imgBodyBbox is not None and len(imgBodyBbox) > 0 and len(imgBodyBbox[0]) > 0
		if args.face:
			imgFaceBbox = imgGetFaceBbox(image, "face_alignment")
			valid_face = imgFaceBbox is not None

		if valid_body and valid_face:
			if args.face and valid_face:
				imgFaceKps = imgGetFaceKeypoints(image, "face_alignment")
			if args.body and valid_body:
				imgBodyKps = imgGetBodyKeypoints(image, "efficient_pose_i")["coordinates"]

			if args.face and valid_face:
				image = bbox_image_plot(image, imgFaceBbox) if not imgFaceBbox is None else image
				image = frame_keypointer(image, imgFaceKps, radius_percent=0.5) if not imgFaceKps is None else image

			if args.body and valid_body:
				image = bbox_image_plot(image, imgBodyBbox[0][0])
				image = frame_keypointer(image, imgBodyKps, radius_percent=0.5) if not imgBodyKps is None else image
				image = imageDisplayBodyParts(image, imgBodyKps) if not imgBodyKps is None else image

			assert image.shape[0] > 0 and image.shape[1] > 0

		frame = cv2.flip(image[..., ::-1], 1)
		frame = cv2.resize(frame, (500, 500))
		cv2.imshow("Camming is fun", frame)			
		
		if cv2.waitKey(1) & 0xFF == ord("q"):
			break
			
	cap.release()
	cv2.destroyAllWindows()

if __name__ == "__main__":
	main()
