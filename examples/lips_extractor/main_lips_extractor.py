import cv2
import numpy as np
from tqdm import trange
from argparse import ArgumentParser
from pathlib import Path

from neural_wrappers.utilities import tryWriteVideo, tryReadVideo, resize, FakeArgs
from neural_wrappers.pytorch import device

from DRN.main_sr import srImage, Model

def getArgs():
	parser = ArgumentParser()
	parser.add_argument("type")
	parser.add_argument("input")
	parser.add_argument("inputKeypoints")
	parser.add_argument("output")

	parser.add_argument("--outputResolution")
	args = parser.parse_args()

	assert args.type == "video_dir"
	if args.output.endswith(".npy"):
		args.output = ".".join(args.output.split(".")[0 : -1])
	if not args.outputResolution is None:
		args.outputResolution = list(map(lambda x : int(x), args.outputResolution.split(",")))
	return args

def imgGetLipsExtremes(lips, percent=20):
	minW, maxW, minH, maxH = lips[:, 0].min(), lips[:, 0].max(), lips[:, 1].min(), lips[:, 1].max()
	
	diffW = maxW - minW
	diffH = maxH - minH
	minH = minH - diffH // percent
	maxH = maxH + diffH // percent
	minW = minW - diffW // percent
	maxW = maxW + diffW // percent

	extremes = np.array([minW, maxW, minH, maxH], dtype=np.int32)
	return extremes

def imgGetLipsCrop(source_image, imgExtremes, srModel, resolution=(256, 256)):
	minW, maxW, minH, maxH = imgExtremes
	diffW = maxW - minW
	# Keep scale by using only width
	centerH = (maxH + minH) / 2
	minH = np.int32(centerH - diffW // 2)
	maxH = np.int32(centerH + diffW // 2)
	minH = np.clip(minH, 0, source_image.shape[0])
	maxH = np.clip(maxH, 0, source_image.shape[0])

	imgLips = source_image[minH : maxH, minW : maxW]
	imgLips = srImage(srModel, np.expand_dims(imgLips, axis=0))[0].astype(np.uint8)
	imgLips = resize(imgLips, height=resolution[0], width=resolution[1], \
		interpolation="bilinear", resizeLib="lycon")
	return imgLips

def vidGetLipsExtremes(lips, percent=(20, 20)):
	N = len(lips)
	extremes = np.zeros((N, 4, ), dtype=np.int32)
	for i in range(N):
		res = imgGetLipsExtremes(lips[i])
		extremes[i] = res
	return extremes

def vidSmoothLipsExtremes(video, extremes):
	# Based on all extremes, get the median frame and work with that
	# Get middle lip
	centerW = (extremes[:, 0] + extremes[:, 1]) // 2
	centerH = (extremes[:, 2] + extremes[:, 3]) // 2
	# Get median of differences
	diffW = np.median(extremes[:, 1] - extremes[:, 0])
	# Based on center +/- diffs, get the extremities of the bbox
	minW = np.int32(centerW - diffW // 2)
	maxW = np.int32(centerW + diffW // 2)
	minH = np.int32(centerH - diffW // 2)
	maxH = np.int32(centerH + diffW // 2)
	# Ensure we are on the image
	minW = np.clip(minW, 0, video.shape[2])
	maxW = np.clip(maxW, 0, video.shape[2])
	minH = np.clip(minH, 0, video.shape[1])
	maxH = np.clip(maxH, 0, video.shape[1])
	# Smooth the result further by doing a running mean with a 10 window
	M = 10
	Range = np.ones(M) / M
	l, r = M // 2, - M // 2 + (M % 2 == 0)
	minW[l : r] = np.convolve(minW, Range, mode="valid")
	maxW[l : r] = np.convolve(maxW, Range, mode="valid")
	minH[l : r] = np.convolve(minH, Range, mode="valid")
	maxH[l : r] = np.convolve(maxH, Range, mode="valid")
	# Stack them back
	extremes = np.stack([minW, maxW, minH, maxH], axis=1).astype(np.int32)

	return extremes

def getLipsFrames(video, extremes, srModel, resolution=(256, 256)):
	assert len(video) == len(extremes)
	N = len(video)
	assert not resolution is None
	# TODO: If resolution is None, it must be inferred from diffs. However, after convolve _a few_ diffs lose their
	#  equal differences :)
	result = np.zeros((N, resolution[0], resolution[1], 3), dtype=np.uint8)

	# print((extremes[:, 1] - extremes[:, 0]) / (extremes[:, 3] - extremes[:, 2]))
	for i in trange(N, desc="Crop"):
		img = video[i]
		minW, maxW, minH, maxH = extremes[i]
		frame = img[minH : maxH, minW : maxW]

		if not resolution is None:
			srFrame = srImage(srModel, np.expand_dims(frame, axis=0))[0].astype(np.uint8)
			# print(srFrame.shape)
			resizedFrame = resize(srFrame, height=resolution[0], width=resolution[1], interpolation="bilinear")
			# print(resizedFrame.shape)
			frame = resizedFrame

		result[i] = frame
	return result

def doVideoDir(args, srModel):
	paths = list(map(lambda x : str(x), Path(args.input).glob("*.mp4")))
	items = list(map(lambda x : x.split("/")[-1][0 : -4], paths))
	kpPaths = ["%s/%s.npy" % (args.inputKeypoints, item) for item in items]

	outPath = Path(args.output)
	outPath.mkdir(exist_ok=True, parents=True)
	N = len(paths)
	for i in trange(N):
		vidPath = paths[i]
		kpPath = kpPaths[i]
		outPath = "%s/%s.mp4" % (args.output, items[i])

		video, fps = tryReadVideo(vidPath)
		try:
			videoKeypoints = np.load(kpPath)
			assert len(video) == len(videoKeypoints)
		except Exception as e:
			print(e)
			continue

		lips = videoKeypoints[:, 48 : 60]
		extremes = vidGetLipsExtremes(lips)
		extremes = vidSmoothLipsExtremes(video, extremes)
		frames = getLipsFrames(video, extremes, srModel, resolution=args.outputResolution)
		tryWriteVideo(frames, outPath, fps=fps)

def main():
	args = getArgs()
	srArgs = FakeArgs({"cpu" : device, "scale" : [pow(2, s+1) for s in range(int(np.log2([4])))], "model" : "DRN-L", \
		"pre_train" : "../DRN/DRNL4x.pt", \
		"test_only" : True, "rgb_range" : 255, "self_ensemble" : "store_true", \
		"n_GPUs" : 1, "n_blocks" : 30, "n_feats" : 20, "n_colors" : 3, "negval" : 0.2, \
		"pre_train_dual" : ".", "video_batch_size" : 4
	})

	srModel = Model(srArgs).to(device).eval()
	srModel.get_model().mode = "4x"
	doVideoDir(args, srModel)

if __name__ == "__main__":
	main()

