
import sys
import numpy as np
from pathlib import Path
from tqdm import trange
from media_processing_lib.video import tryReadVideo
from vke.face import vidGetFaceBbox, vidGetFaceKeypoints
from shutil import copyfile

def main():
    files = [str(x) for x in Path(sys.argv[1]).glob("*.mp4")]
    print("Extracting face bounding box for %d mp4 videos from %s" % (len(files), sys.argv[1]))

    p = Path("results")
    p.mkdir(exist_ok=True)
    (p / "videos").mkdir(exist_ok=True)
    (p / "keypoints").mkdir(exist_ok=True)

    for i in trange(len(files)):
        if i > 100:
            break
        video = tryReadVideo(files[i], vidLib="pims")
        faceKps = vidGetFaceKeypoints(video, faceLib="face_alignment")

        assert len(faceKps) == len(video)
        videoName = files[i].split("/")[-1]
        kpName = videoName.split(".")[0] + ".npy"

        copyfile(files[i], (p / "videos" / videoName))
        np.save((p / "keypoints" / kpName), faceKps)

if __name__ == "__main__":
    main()
