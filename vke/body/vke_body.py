from abc import ABC, abstractmethod

class VKE_Body(ABC):
    @abstractmethod
    def imgGetBodyKeypoints(image):
        pass

    @abstractmethod
    def imgGetBodyBbox(image):
        pass
    
    @abstractmethod
    def vidGetBodyKeypoints(video):
        pass

    @abstractmethod
    def vidGetBodyBbox(video):
        pass
