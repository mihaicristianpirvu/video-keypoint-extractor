import os
import sys
import subprocess
import torch as tr
import numpy as np
from functools import partial
from tqdm import trange
from overrides import overrides
from pathlib import Path
from nwutils.numpy import np_get_data
from nwutils.torch import tr_get_data, tr_to_device
from ...utils import videoGetDescriptor, vkeCache, drange, dprint
from ..vke_body import VKE_Body

device = tr.device("cuda") if tr.cuda.is_available() else tr.device("cpu")

def doInference(image, model, non_max_suppression, conf_thres=0.4, iou_thres=0.5):
	classes = ['person', 'bicycle', 'car', 'motorcycle', 'airplane', 'bus', 'train', 'truck', 'boat', 'traffic light', \
		'fire hydrant', 'stop sign', 'parking meter', 'bench', 'bird', 'cat', 'dog', 'horse', 'sheep', 'cow', \
		'elephant', 'bear', 'zebra', 'giraffe', 'backpack', 'umbrella', 'handbag', 'tie', 'suitcase', 'frisbee', \
		'skis', 'snowboard', 'sports ball', 'kite', 'baseball bat', 'baseball glove', 'skateboard', 'surfboard', \
		'tennis racket', 'bottle', 'wine glass', 'cup', 'fork', 'knife', 'spoon', 'bowl', 'banana', 'apple', \
		'sandwich', 'orange', 'broccoli', 'carrot', 'hot dog', 'pizza', 'donut', 'cake', 'chair', 'couch', \
		'potted plant', 'bed', 'dining table', 'toilet', 'tv', 'laptop', 'mouse', 'remote', 'keyboard', 'cell phone', \
		'microwave', 'oven', 'toaster', 'sink', 'refrigerator', 'book', 'clock', 'vase', 'scissors', \
		'teddy bear', 'hair drier', 'toothbrush']

	npImage = np.float32(image) / 255
	npImage = npImage.transpose(2, 0, 1)[None]
	trImage = tr_to_device(tr_get_data(npImage), device)
	pred = model(trImage, augment="store_true")[0]
	pred_nms = non_max_suppression(pred, conf_thres, iou_thres, classes=None, agnostic="store_true")[0]
	if pred_nms is None:
		return None
	# modelc = tr.hub.load('pytorch/vision:v0.6.0', 'resnet101', pretrained=True).eval()
	# pred_cls = apply_classifier(pred_nms, modelc, trImage, trImage)

	pred_bbox = pred_nms[..., 0 : 4]
	pred_conf = pred_nms[..., 4 : 5]
	pred_cls = pred_nms[..., 5]

	person = classes.index("person")
	wherePerson = pred_cls == person
	# pred_bbox is an array of [nBboxes x 4] represented as x1, y1, x2, y2. We convert to x1, x2, y1, y2
	pred_bbox = np_get_data(pred_bbox[wherePerson])
	pred_bbox = pred_bbox[..., [0, 2, 1, 3]].astype(np.int32)
	pred_conf = np_get_data(pred_conf[wherePerson])

	return pred_bbox, pred_conf

def getModel(mode):
	# class Dummy: pass
	# model = Dummy()
	# model.doInference = lambda image, model, non_max_suppression: (0, 0, 0, 0), 0

	baseDir = os.path.dirname(os.path.abspath(os.path.realpath(__file__)))
	yoloDir = "%s/yolo_repo" % baseDir
	print("[yolov5::getModel] Getting model. Mode: %s" % mode)
	if not Path(yoloDir).exists():
		cwd = os.getcwd()

		os.chdir(baseDir)
		print("[yolov5::getModel] Repository not present, cloning and downloading weights...")
		cmd = "git clone https://github.com/ultralytics/yolov5/ yolo_repo".split(" ")
		pipe = subprocess.Popen(cmd)
		pipe.wait()

		cwd = os.getcwd()
		os.chdir(yoloDir)
		cmd = "bash weights/download_weights.sh".split(" ")
		pipe = subprocess.Popen(cmd)
		pipe.wait()
		os.chdir(cwd)

	cfg = {
		"S" : "%s/models/yolov5s.yaml" % yoloDir
	}[mode]

	weightsFile = {
		"S" : "%s/yolov5s.pt" % yoloDir
	}[mode]

	sys.path.insert(0, yoloDir)
	from models.yolo import Model
	from utils.general import non_max_suppression

	print("[yolov5::getModel] Loading weights.")
	model = Model(cfg=cfg)
	# Technically, we can just return the file here, but I'd rather instantiate it manually to be sure.
	state_dict = tr.load(weightsFile)["model"].state_dict()
	model.load_state_dict(state_dict)
	model.doInference = partial(doInference, model=model, non_max_suppression=non_max_suppression)
	model = model.to(device).eval()

	sys.path.pop(0)
	return model

class VKE_YOLOV5(VKE_Body):
	def __init__(self, mode):
		assert mode in ("S", )
		self.mode = mode
		self.model = getModel(mode)

	@overrides
	def imgGetBodyBbox(self, image):
		return self.model.doInference(image)

	@overrides
	def vidGetBodyBbox(self, video):
		key = "%s/yolov5s_body_bbox.npy" % videoGetDescriptor(video)
		if vkeCache.check(key):
			vidKeypoints = vkeCache.get(key)
			assert len(vidKeypoints) == len(video)
			dprint("[%s::vidGetBodyBbox] Loaded bbox from cache: %s" % (str(self), key))
			return vidKeypoints

		N = len(video)
		# doInference can return multiple human boxes per frame.
		res = np.zeros((N, ), dtype=np.object)
		nones = []
		for i in drange(N, desc=str(self)):
			frame = video[i]
			item = self.model.doInference(frame)
			if item is None:
				nones.append(i)
				if i > 0:
					res[i] = res[i - 1]
				continue
			pred_bbox, pred_conf = item
			res[i] = pred_bbox

		if len(nones) > N * 100 / 5:
			dprint("[%s::vidGetBodyBbox] More than 5%% of video has no bbox. Skipping." % str(self))
			return None

		dprint("[%s::vidGetBodyBbox] Stored bbox to: %s" % (str(self), key))
		vkeCache.set(key, res)
		return res

	@overrides
	def imgGetBodyKeypoints(self, image):
		assert False

	@overrides
	def vidGetBodyKeypoints(self, video):
		assert False

	def __str__(self):
		return "yolov5-%s" % self.mode