import numpy as np
import cv2
from ..utils import drange

def hex2int(hex:str):
	assert len(hex) == 7
	a, b, c = hex[1 : 3], hex[3: 5], hex[5: 7]
	return [int(a, 16), int(b, 16), int(c, 16)]

def imageDisplayBodyParts(image, imgKeypoints):
	# Define segments and colors
	body_parts = ['head_top', 'upper_neck', 'right_shoulder', 'right_elbow', 'right_wrist', 'thorax', \
		'left_shoulder', 'left_elbow', 'left_wrist', 'pelvis', 'right_hip', 'right_knee', \
		'right_ankle', 'left_hip', 'left_knee', 'left_ankle']
	segments = [(0, 1), (1, 5), (5, 2), (5, 6), (5, 9), (2, 3), (3, 4), (6, 7), (7, 8), (9, 10), \
		(9, 13), (10, 11), (11, 12), (13, 14), (14, 15)]
	segment_colors = ["#fff142", "#fff142", "#576ab1", "#5883c4", "#56bdef", "#f19718", "#d33592", \
		"#d962a6", "#e18abd", "#f19718", "#8ac691", "#a3d091", "#bedb8f", "#7b76b7", "#907ab8", "#a97fb9"]
	segment_colors = [hex2int(x) for x in segment_colors]
	H, W = image.shape[0], image.shape[1]

	thickness = max(1, min(H, W) // 200)
	for i in range(len(segments)):
		body_part_a_index, body_part_b_index = segments[i]
		color = segment_colors[i]
		body_part_a_x, body_part_a_y = imgKeypoints[body_part_a_index]
		body_part_b_x, body_part_b_y = imgKeypoints[body_part_b_index]
		image = cv2.line(image, (body_part_a_x, body_part_a_y), (body_part_b_x, body_part_b_y), \
			color=color, thickness=thickness)
	return image

def videoDisplayBodyParts(video, bodyKeypoints):	
	res = []
	for i in drange(len(video), desc="Body skeleton video"):
		imgKeypoints = bodyKeypoints[i]
		frame = video[i]
		frame = imageDisplayBodyParts(frame, imgKeypoints)
		res.append(frame)
	return np.array(res)