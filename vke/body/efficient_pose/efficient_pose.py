import os
import numpy as np
import torch as tr
from functools import partial

from imp import load_source
from overrides import overrides
from .pose_utils import vidGetBodyKeypoints, imgGetBodyKeypoints
from ..vke_body import VKE_Body

device = tr.device("cuda") if tr.cuda.is_available() else tr.device("cpu")

def doInference(batch, model):
    batch = np.rollaxis(batch, 3, 1)
    batch = tr.from_numpy(batch).float().to(device)
    with tr.no_grad():
        batch_outputs = model(batch)
    batch_outputs = batch_outputs.detach().to("cpu").numpy()
    batch_outputs = np.rollaxis(batch_outputs, 1, 4)

    return batch_outputs

class VKE_EfficientPose(VKE_Body):
    def __init__(self, mode):
        assert mode in ("RT", "I", "II", "III", "IV")
        self.mode = mode
        self.model = self.getModel()

    def getModel(self):
        baseDir = os.path.abspath(os.path.realpath(os.path.dirname(__file__)))
        if self.mode == "RT":
            modelPath = "%s/weights/EfficientPoseRT" % baseDir
            MainModel = load_source("MainModel", "%s/models/EfficientPoseRT.py" % baseDir)
        elif self.mode == "I":
            modelPath = "%s/weights/EfficientPoseI" % baseDir
            MainModel = load_source("MainModel", "%s/models/EfficientPoseI.py" % baseDir)
        elif self.mode == "II":
            modelPath = "%s/weights/EfficientPoseII" % baseDir
            MainModel = load_source("MainModel", "%s/models/EfficientPoseII.py" % baseDir)
        elif self.mode == "III":
            modelPath = "%s/weights/EfficientPoseIII" % baseDir
            MainModel = load_source("MainModel", "%s/models/EfficientPoseIII.py" % baseDir)
        elif self.mode == "IV":
            modelPath = "%s/weights/EfficientPoseIV" % baseDir
            MainModel = load_source("MainModel", "%s/models/EfficientPoseIV.py" % baseDir)

        model = tr.load(modelPath)
        model.to(device)
        model.eval()
        qconfig = tr.quantization.get_default_qconfig("qnnpack")
        tr.backends.quantized.engine = "qnnpack"

        model.modelType = self.mode
        model.resolution = {"RT": 224, "I": 256, "II": 368, "III": 480, "IV": 600}[self.mode]
        model.doInference = partial(doInference, model=model)
        return model

    @overrides
    def vidGetBodyKeypoints(self, video):
        return vidGetBodyKeypoints(video, self.model)

    @overrides
    def imgGetBodyKeypoints(self, image):
        return imgGetBodyKeypoints(image, self.model)

    @overrides
    def imgGetBodyBbox(self, image):
        assert False

    @overrides
    def vidGetBodyBbox(self, video):
        assert False
